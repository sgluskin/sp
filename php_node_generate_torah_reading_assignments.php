<?php
/*

This is a script to create job_instance nodes
for each torah instance given a list of Day at Shul nodes.

It can be run via drush like so:

  drush scr php_node_generate_torah_reading_assignments.php

If the script is not in the path of the webroot, you may have
add the 'script-path' parameter.

  drush scr php_node_generate_torah_reading_assignments.php --script-path=/full/path/to/script

It is important to note that it will create new nodes each
time it is run, so if a Day at Shul node is run twice, then
it will create double the nodes.

*/

use Drupal\node\Entity\Node;

// TODO: There are a few different triennial year values to run through.
// In the future this could be configurable via passed-in-parameter.
$triennial_year = 'Third';

$dates = [
  19367
/*
  19368,
  19369,
  19370,
  19371,
  19372,
  19373,
  19374,
  19375,
  19376,
  19377,
  19378,
  19379,
  19380,
  19381,
  19382,
  19383,
  19384,
  19385,
  19386,
  19387,
  19388,
  19389,
  19390,
  19391,
  19392,
  19393,
  19394,
  19395,
  19396,
  19397,
  19398,
  19399,
  19400,
  19401,
  19402,
  19403,
  19404 */
];

foreach ($dates as $date_nid) {
  // Find the related torah reading node ids for the given Day at Shul.
  // The entity query runs a query based on fields and properties
  // of an entity so users don't need to be aware of the mysql table
  // setup.
  // A user can figure out the field conditions by printing out a node
  // via devel and looking at the data structure on the given field.
  $date_node = Node::load($date_nid);
  $torahreadings = \Drupal::entityQuery('node')
                     ->condition('type', 'torah_reading')
                     ->condition('status', 1)
                     ->condition('field_parashah_list.value', $date_node->field_parashah->value)
                     // The triennial_year value can be changed above.
                     ->condition('field_triennial_year.value', $triennial_year)
                     ->execute();
  \Drupal::messenger()->addMessage(sprintf("Creating job instances for %d", $date_nid));

  if (!empty($torahreadings)) {
    // For each torah reading node found, we need to create a new job instance.
    foreach ($torahreadings as $torahreading_nid) {
      $torahreading = Node::load($torahreading_nid);

      $node = Node::create(['type' => 'job_instance']);
      // The title of the node is based on fields from the toarh reading node.
      $node->title->value = $torahreading->field_reading_job_name->value . ': ' . $torahreading->title->value . ' (' . $torahreading->field_verse_count->value . ' verses)';
      $node->field_day_at_shul->target_id = $date_nid;
      $node->field_job_or_reading->target_id = $torahreading_nid;
      $node->uid = 3;
      $node-> enforceIsNew();
      $node-> save();

      \Drupal::messenger()->addMessage(sprintf("Created node %d, %s", $node->id(), $node->title->value));
    }
  }
}
