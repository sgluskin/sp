<?php
// this code worked; it generated 2600 new job_instance nodes -- 40 dates * 65 jobs
use Drupal\node\Entity\Node;

// Set properties to filter.
$values = [
  'type' => 'day_at_shul',
];

// Get the nodes.
$nodes = \Drupal::entityTypeManager()
  ->getStorage('node')
  ->loadByProperties($values);
// dvr($nodes);

$jobs = array(
  21986,
  21987,
  21988,
  21989,
  21990,
  21991,
  21992,
  21993,
  21994,
  21995,
  21996,
  21997,
  21998,
  21999,
  22000,
  22001,
  22002,
  22003,
  22004,
  22005,
  22006,
  22007,
  22008,
  22009,
  22010,
  22011,
  22012,
  22013,
  22014,
  22015,
  22016,
  22017,
  22018,
  22019,
  22020,
  22021,
  22022,
  22023,
  22024,
  22025,
  22026,
  22027,
  22028,
  22029,
  22030,
  22031,
  22032,
  22033,
  22034,
  22035,
  22036,
  22037,
  22038,
  22039,
  22040,
  22041,
  22042,
  22043,
  22044,
  22045,
  22046,
  22047,
  22048,
  22049,
  22050
);
$dates = array(
  19367
/*  19368,
  19369,
  19370,
  19371,
  19372,
  19373,
  19374,
  19375,
  19376,
  19377,
  19378,
  19379,
  19380,
  19381,
  19382,
  19383,
  19384,
  19385,
  19386,
  19387,
  19388,
  19389,
  19390,
  19391,
  19392,
  19393,
  19394,
  19395,
  19396,
  19397,
  19398,
  19399,
  19400,
  19401,
  19402,
  19403,
  19404 */
);
foreach ($dates as $date) {
  foreach ($jobs as $job) {

    $node = Node::create(['type' => 'job_instance']);
    $node->field_day_at_shul->target_id = $date;
    $node->field_job_or_reading->target_id = $job; 
    $node->uid = 3; 
    $node-> enforceIsNew();
    $node-> save();
  }
}

