<?php

namespace Drupal\shabbat_people\Controller;

use Drupal\shabbat_people\ShabbatScheduler;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller scheduler related pages.
 */
class Scheduler extends ControllerBase {

  /**
   * The shabbat scheduler service.
   *
   * @var \Drupal\shabbat_people\ShabbatScheduler
   */
  protected $scheduler;

  /**
   * The currently active request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Scheduler controller constructor.
   *
   * @param \Drupal\shabbat_people\ShabbatScheduler $scheduler
   *   The shabbat scheduler service.
   * @param Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(ShabbatScheduler $scheduler, RequestStack $request_stack) {
    $this->scheduler = $scheduler;
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('shabbat_people.scheduler'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getModuleName() {
    return 'shabbat_people';
  }

  /**
   * Page for displaying scheduler form.
   */
  public function schedulerForm() {
    $build = [
      '#markup' => $this->t('Schedule the days on which you will meet.'),
    ];
    $build['scheduler_form'] = $this->formBuilder()->getForm('Drupal\shabbat_people\Form\SchedulerForm');
    return $build;
  }

}

