<?php
/**
 * @file
 * Contains \Drupal\shabbat_people\Form\SchedulerForm.
 */

namespace Drupal\shabbat_people\Form;

use Drupal\shabbat_people\ShabbatScheduler;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Scheduler form for creating job instances.
 */
class SchedulerForm extends FormBase {

  /**
   * The scheduler service.
   *
   * @var \Drupal\shabbat_people\ShabbatScheduler
   */
  protected $scheduler;

  /**
   * Creates a SchedulerForm instance.
   *
   * @param \Drupal\shabbat_people\ShabbatScheduler $scheduler
   *   The scheduler service.
   */
  public function __construct(ShabbatScheduler $scheduler) {
    $this->scheduler = $scheduler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('shabbat_people.scheduler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
		return 'shabbat_people_scheduler_form';
  }

  /**
   * Get the scheduler service.
   *
   * @return \Drupal\shabbat_people\ShabbatScheduler
   */
  public function getScheduler() {
    return $this->scheduler;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $unassigned_days = $this->getScheduler()->getUnassignedDays();
    if (!empty($unassigned_days)) {
      foreach ($unassigned_days as $day) {
        $options[$day->id()] = $this->t($day->title->value);
      }
			$form['unassigned_days'] = [
				'#title' => $this->t('Unassigned Days at Shul'),
				'#type' => 'checkboxes',
				'#options' => $options,
				'#default_value' => [],
			];
			$form['submit'] = [
				'#type' => 'submit',
				'#value' => t('Submit'),
			];
    }
    else {
      $form['empty'] = [
        '#type' => 'markup',
        '#markup' => $this->t('There are currently no unassigned Days at Shul to schedule.'),
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $batch = [
      'title' => t('Creating Job Instances...'),
      'operations' => [],
      'progress_message' => t('Processed @current out of @total.'),
      'error_message'    => t('An error occurred during processing'),
      'finished' => '\Drupal\shabbat_people\SchedulerBatch::finishedCallback',
    ];
    foreach ($values['unassigned_days'] as $nid => $value) {
      if ($value) {
        $batch['operations'][] = ['\Drupal\shabbat_people\SchedulerBatch::operationCallback',[$nid]];
      }
    }

    batch_set($batch);
  }

}
