<?php

namespace Drupal\shabbat_people;

use Drupal\node\Entity\Node;

/**
 * Class for running batch code related to the scheduler.
 *
 * Note that batch code has to run in procedural code
 * or in a class with static methods.
 */
class SchedulerBatch {

  /**
   * The scheduler callback for batch operation.
   *
   * @param int $nid
   *  The node id of the day at shul from whic to create job instances.
   *
   * @param array $context
   *  Array containing the stored context for the batch operation.
   */
  public static function operationCallback($nid, &$context) {
    $scheduler = \Drupal::service('shabbat_people.scheduler');
    if (empty($context['results'])) {
      $context['results'] = [];
    }

    $day_at_shul = Node::load($nid);
    $job_instances = $scheduler->createJobInstances($day_at_shul);
    $results = array_map(function($job_instance) {
      return $job_instance->id();
    }, $job_instances);

    $message = t(sprintf('Creating job instances for %s', $day_at_shul->title->value));
    $context['message'] = $message;
    $context['results'] = array_merge($context['results'], $results);
  }

  /**
   * The scheduler callback for when it's finished.
   *
   * @param bool $success
   *   Whether or not the batch operation was successful.
   *
   * @param array $results
   *  List of results of batch operations.
   *
   * @param array $operations
   *  List of operations run during batch.
   */
  public static function finishedCallback($success, $results, $operations) {
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One job instance created.', '@count job instances created.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addStatus($message);
    \Drupal::logger('shabbat_people')->notice($message);
  }
}
