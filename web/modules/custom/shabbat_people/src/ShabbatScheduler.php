<?php

namespace Drupal\shabbat_people;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * Service for the scheduler data handling.
 */
class ShabbatScheduler {

  use EntityQueryTrait;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * ShabbatScheduler constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger channel.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerChannelInterface $logger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
  }

  /**
   * Get list of Day at Shul nodes that have no job instances assigned.
   *
   * @return array
   *   List of node of unassigned Day at Shul nodes.
   */
  public function getUnassignedDays() {
    $nodes = [];
    $nids = $this->entityQuery('node')
                ->condition('type', 'day_at_shul')
                ->condition('status', 1)
                ->condition('field_jobs_assigned', 1, '!=')
                ->condition('field_date', date('Y-m-d'), '>=')
                ->execute();
    if (!empty($nids)) {
      $nodes = $this->getEntityTypeManager()->getStorage('node')->loadMultiple($nids);
    }
    return $nodes;
  }

  /**
   * Mark that jobs have been assigned to the given Day at Shul.
   *
   * @param NodeInterface $day_at_shul
   *   A loaded day_at_shul node to mark as assigned.
   *
   * @return int
   *  Either SAVED_NEW or SAVED_UPDATED, depending on the operation performed.
   */
  public function markJobsAssigned(NodeInterface $day_at_shul) {
    $day_at_shul->field_jobs_assigned->value = 1;
    return $day_at_shul->save();
  }

  /**
   * Create all job instances for a Day at Shul.
   *
   * @param NodeInterface $day_at_shul
   *   A loaded day_at_shul node to mark as assigned.
   *
   * @return array
   *   A list of loaded job instances created.
   */
  public function createJobInstances(NodeInterface $day_at_shul) {
    $job_instances = [];

    // First create all instances from the existing jobs.
    $job_nids = $this->getJobs($day_at_shul);
    if (!empty($job_nids)) {
      $jobs = $this->getEntityTypeManager()->getStorage('node')->loadMultiple($job_nids);
      foreach ($jobs as $job) {
        $job_instances[$job->id()] = $this->createJobInstance($day_at_shul, $job);
      }
    }

    // Next create all instances from the related torah readings.
    $torah_readings_nids = $this->getTorahReadings($day_at_shul);
    if (!empty($torah_readings_nids)) {
      $torah_readings = $this->getEntityTypeManager()->getStorage('node')->loadMultiple($torah_readings_nids);
      foreach ($torah_readings as $torah_reading) {
        $title = sprintf('%s: %s (%s verses)', $torah_reading->field_reading_job_name->value, $torah_reading->title->value, $torah_reading->field_verse_count->value);
        $job_instances[$torah_reading->id()] = $this->createJobInstance($day_at_shul, $torah_reading, $title);
      }
    }

    // Next create all instances from the related haftarah readings.
    $maftir_readings_nids = $this->getmaftirReadings($day_at_shul);
    if (!empty($maftir_readings_nids)) {
      $maftir_readings = $this->getEntityTypeManager()->getStorage('node')->loadMultiple($maftir_readings_nids);
      foreach ($maftir_readings as $maftir_reading) {
        $title = sprintf('Maftir Aliyah, Reader: %s (%s verses)', $maftir_reading->title->value, $maftir_reading->field_verse_count->value);
        $job_instances[$maftir_reading->id()] = $this->createJobInstance($day_at_shul, $maftir_reading, $title);
      }
    }

    // Next create all instances from the related haftarah readings.
    $haftarah_readings_nids = $this->getHaftarahReadings($day_at_shul);
    if (!empty($haftarah_readings_nids)) {
      $haftarah_readings = $this->getEntityTypeManager()->getStorage('node')->loadMultiple($haftarah_readings_nids);
      foreach ($haftarah_readings as $haftarah_reading) {
        $title = sprintf('Haftarah Reader: %s (%s verses)', $haftarah_reading->title->value, $haftarah_reading->field_verse_count->value);
        $job_instances[$haftarah_reading->id()] = $this->createJobInstance($day_at_shul, $haftarah_reading, $title);
      }
    }

    // If we get this far, then the day has been assigned with job instances.
    $this->markJobsAssigned($day_at_shul);

    return $job_instances;
  }

  /**
   * Get the jobs needed for creating job instances.
   *
   * @param NodeInterface $day_at_shul
   *   A loaded day_at_shul node to mark as assigned.
   *
   * @return array
   *  List of job node ids.
   */
  public function getJobs(NodeInterface $day_at_shul) {
    return $this->entityQuery('node')
                ->condition('type', 'jobs')
                ->condition('status', 1)
                // Only select jobs that are marked for the occasion on the day at shul.
                ->condition('field_job_occasion.target_id', $day_at_shul->field_job_occasion_day->target_id)
	        ->condition('field_skip_scheduler', 1, '!=') // Only select jobs not marked as "Skip scheduler" which are meant for manual node/add/job_instance

                ->execute();
  }

  /**
   * Get the torah readings needed for creating job instances.
   *
   * @param NodeInterface $day_at_shul
   *   A loaded day_at_shul node.
   *
   * @return array
   *  List of torah reading node ids.
   */
  public function getTorahReadings(NodeInterface $day_at_shul) {
    $holiday_id = 70;
    $query = $this->entityQuery('node')
                  ->condition('type', 'torah_readings')
                  ->condition('status', 1)
                  ->condition('field_torah_occasion.value', $day_at_shul->field_torah_occasion->value);
    // Holidays have values at 10000 and above and don't require a triennial year.
    if ((int) $day_at_shul->field_torah_occasion->value < $holiday_id) {
      $query->condition('field_triennial_year.value', $day_at_shul->field_triennial_year->value);
    }
    return $query->execute();
  }

  /**
   * Get the maftir readings needed for creating job instances.
   *
   * @param NodeInterface $day_at_shul
   *   A loaded day_at_shul node.
   *
   * @return array
   *  List of maftir reading node ids.
   */
  public function getMaftirReadings(NodeInterface $day_at_shul) {
    $holiday_id = 100;
    $query = $this->entityQuery('node')
                 ->condition('type', 'maftir_reading')
                 ->condition('status', 1)
                 ->condition('field_maftir_occasion.value', $day_at_shul->field_maftir_occasion->value);
    // Holidays have values at 100 and above and don't require a triennial year.
    if ($day_at_shul->field_maftir_occasion->value < $holiday_id) {
      $query->condition('field_triennial_year.value', $day_at_shul->field_triennial_year->value);
    }
    return $query->execute();
  }

  /**
   * Get the haftarah readings needed for creating job instances.
   *
   * @param NodeInterface $day_at_shul
   *   A loaded day_at_shul node.
   *
   * @return array
   *  List of haftarah reading node ids.
   */
  public function getHaftarahReadings(NodeInterface $day_at_shul) {
    return $this->entityQuery('node')
                ->condition('type', 'haftarah_reading')
                ->condition('status', 1)
                ->condition('field_haftarah_occasion.value', $day_at_shul->field_haftarah_occasion->value)
                ->execute();
  }

  /**
   * Create job instances for a given job/reading node and day at shul.
   *
   * @param NodeInterface $day_at_shul
   *   A loaded day_at_shul node.
   *
   * @param NodeInterface $job_or_reading
   *   A loaded job or reading type node that can be referenced by job instance.
   *
   * @param string $title
   *   (Optional) The title to set on the new node.
   *   If no title set, it will use the automatic node title.
   *
   * @return NodeInterface
   *   The newly created job instance node.
   */
  public function createJobInstance(NodeInterface $day_at_shul, NodeInterface $job_or_reading, string $title = NULL) {
    $node = Node::create(['type' => 'job_instance']);
    $node->field_day_at_shul->target_id = $day_at_shul->id();
    $node->field_job_or_reading->target_id = $job_or_reading->id();

    // Set the tile if it's passed in.
    // Automatic node title will be used if not set.
    if ($title) {
      $node->title->value = $title;
    }

    // Hide/show the job instance to members depending on the parent node setting.
    if ($job_or_reading->hasField('field_show_hide_from_members')) {
      $node->field_show_hide_from_members->value = $job_or_reading->field_show_hide_from_members->value;
    }

    // Note: this node will be authored by the logged-in user.
    $node->enforceIsNew();
    $node->save();
    $this->logger->notice('Created job instance @id',['@id' => $node->id()]);
    return $node;
  }

}
