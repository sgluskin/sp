<?php

namespace Drupal\Shabbat_people;

/**
 * Trait to standardize running an entityQuery in the services.
 */
trait EntityQueryTrait {

 /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

 /**
   * Returns the entity query object for this entity type.
   *
   * @param string $entity_type
   *   The entity type (for example, node) for which the query object should be
   *   returned.
   * @param string $conjunction
   *   (optional) Either 'AND' if all conditions in the query need to apply, or
   *   'OR' if any of them is sufficient. Defaults to 'AND'.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   The query object that can query the given entity type.
   */
  public function entityQuery($entity_type, $conjunction = 'AND') {
    return $this->getEntityTypeManager()->getStorage($entity_type)->getQuery($conjunction);
  }

  /**
   * Get the entity type manager service.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager service.
   */
  public function getEntityTypeManager() {
    if (!$this->entityTypeManager) {
      $this->entityTypeManager = \Drupal::service('@entity_type.manager');
    }
    return $this->entityTypeManager;
  }
}
