<?php

namespace Drupal\shabbat_people;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\message\Entity\Message;
use Drupal\message\MessageInterface;
use Drupal\message_notify\MessageNotifier;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * Service for the job instance assignments.
 */
class ShabbatAssigner {

  use EntityQueryTrait;

  /**
   * The message notifier service.
   *
   * @var \Drupal\message_notify\MessageNotifier
   */
  protected $notifier;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The message notification queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * The shabbat_people config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * ShabbatScheduler constructor.
   *
   * @param \Drupal\message_notify\MessageNotifier $notifier
   *   The message notifier service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger channel.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(MessageNotifier $notifier, EntityTypeManagerInterface $entity_type_manager, LoggerChannelInterface $logger, QueueFactory $queue_factory, ConfigFactoryInterface $config_factory) {
    $this->notifier = $notifier;
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->queue = $queue_factory->get('shabbat_people_notifier');
    $this->config = $config_factory->getEditable('shabbat_people.settings');
  }

  /**
   * Determine if the job instance is occuring in the future.
   *
   * @param NodeInterface $job_instance
   *   A loaded job_instance node.
   *
   * @return bool
   *   Whether or not the job_instance occurs in the future.
   */
  public function jobInstanceInFuture(NodeInterface $job_instance) {
    $in_future = FALSE;
    if ($day_at_shul = $job_instance->field_day_at_shul->entity) {
      if ($day_at_shul->field_date->value) {
        $in_future = date('Y-m-d') < $day_at_shul->field_date->value;
      }
    }
    return $in_future;
  }

  /**
   * Determine whether the assignment on a Job Instance has changed.
   *
   * @param NodeInterface $current_job_instance
   *   The job_instance node after the change.
   * @param NodeInterface $original_job_instance
   *   The job_instance node before the change.
   *
   * @return bool
   *  Whether or not the assignment has changed.
   */
  public function assignmentChanged(NodeInterface $current_job_instance, NodeInterface $original_job_instance) {
    $current_assignment = $current_job_instance->field_assigned_to->target_id;
    $original_assignment = $original_job_instance->field_assigned_to->target_id;
    return $current_assignment != $original_assignment;
  }

  /**
   * Create messages for a Job Instance that has changed assignments.
   *
   * @param NodeInterface $current_job_instance
   *   The job_instance node after the change.
   * @param NodeInterface $original_job_instance
   *   The job_instance node before the change.
   */
  public function createMessages(NodeInterface $current_job_instance, NodeInterface $original_job_instance) {
    // The assumption is that the assignment has changed since that's
    // the reason to send this notification.
    $current_assignment = $current_job_instance->field_assigned_to->target_id;
    $original_assignment = $original_job_instance->field_assigned_to->target_id;
    $notifications_monitors = $this->getUsersOfRole('notifications_monitor');

    if (!empty($current_assignment)) {
      $this->createAssignmentMessage('job_assigned_member', $current_assignment, $current_job_instance);
      if (!empty($notifications_monitors)) {
        foreach ($notifications_monitors as $notifications_monitor) {
          $this->createAssignmentMessage('notifications_monitor_job_assign', $notifications_monitor, $current_job_instance);
        }
      }
    }

    if (!empty($original_assignment)) {
      $this->createAssignmentMessage('job_unassigned_member', $original_assignment, $current_job_instance);
      if (!empty($notifications_monitors)) {
        foreach ($notifications_monitors as $notifications_monitor) {
          $this->createAssignmentMessage('notifications_monitor_job_unassi', $notifications_monitor, $current_job_instance);
        }
      }
    }
  }

  /**
   * Create an assignment message for a Job Instance.
   *
   * @param string $type
   *  The bundle name of the type of message to create. It should be a bundle
   *  the contains the field_job_instance field.
   * @param int #uid
   *  The id of the user to whom the message will be sent.
   * @param NodeInterface $job_instance
   *  The job_instance node to which this message will relate.
   * @param bool $queue
   *  Whether to send the notification immediately or to throw into queue.
   *
   * @return \Drupal\message\MessageInterface
   *  The message entity that was created.
   */
  public function createAssignmentMessage(string $type, int $uid, NodeInterface $job_instance, $queue = FALSE) {
    $message = Message::create(['template' => $type]);
    $message->setOwnerId($uid);
    $message->field_job_instance = $job_instance;
    $message->save();
    $this->logger->notice('Message of type %type created for %uid on job instance %id', ['%type' => $type, '%uid' => $uid, '%id' => $job_instance->id()]);

    if ($queue) {
      $this->queueMessage($message);
    }
    else {
      $this->sendMessage($message);
    }
    return $message;
  }

  /**
   * Send the notification on a message.
   *
   * @param \Drupal\message\MessageInterface $message
   *  The message to send.
   */
  public function sendMessage(MessageInterface $message) {
    $this->notifier->send($message, []);
  }

  /**
   * Queue a message notification.
   *
   * @param \Drupal\message\MessageInterface $message
   *  The message to queue.
   *
   * @return
   *   @see QueueInterface::createItem
   */
  public function queueMessage(MessageInterface $message) {
    $item = new \stdClass();
    $item->message_id = $message->id();
    return $this->queue->createItem($item);
  }

  /**
   * Send notifications on the given Job Instances.
   *
   * @param array $job_instance_ids
   *  A list of Job Instance node ids.
   */
  public function sendJobInstanceNotifications(array $job_instance_ids) {
    $job_instances = Node::loadMultiple($job_instance_ids);
    foreach ($job_instances as $job_instance) {
      $this->createAssignmentMessage('assigned_member_reminder', $job_instance->field_assigned_to->target_id, $job_instance, TRUE);
    }
  }

  /**
   * Get a list of Job Instances due for reminder notifications.
   *
   * @return array
   *  A list of Job Instance node ids.
   */
  public function getJobInstancesForReminders() {
    // We send a notification to assigned users a certain number of days
    // ahead of the actual job instance.
    $days_ahead = $this->config->get('reminder_notifications_days_ahead');
    $timezone = date_default_timezone_get();

    $date = new \DateTime('now + ' . $days_ahead . ' days', new \DateTimeZone($timezone));
    $date->setTimezone(new \DateTimeZone(DateTimeItem::STORAGE_TIMEZONE));
    $date = DrupalDateTime::createFromDateTime($date);

    return $this->entityQuery('node')
                ->condition('type', 'job_instance')
                ->condition('status', 1)
                ->exists('field_assigned_to')
                ->condition('field_day_at_shul.entity:node.field_date', $date->format(DateTimeItem::DATE_STORAGE_FORMAT))
                ->execute();
  }


  /**
   * Get a list of users with the given role.
   *
   * @param string $role_id
   *   The machine name of the role.
   *
   * @return array
   *   A list of user entity ids.
   */
  public function getUsersOfRole(string $role_id) {
    // Select only active users, based on the roles given.
    return $this->entityQuery('user')
      ->condition('status', 1)
      ->condition('roles', $role_id)
      ->execute();
  }

}
