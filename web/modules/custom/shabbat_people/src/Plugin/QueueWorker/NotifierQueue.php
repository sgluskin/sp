<?php

namespace Drupal\shabbat_people\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\message_notify\MessageNotifier;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Queue to send message notifications.
 *
 * @QueueWorker(
 *   id = "shabbat_people_notifier",
 *   title = @Translation("Shabbat People Notifier Queue"),
 *   cron = {"time" = 30}
 * )
 */
class NotifierQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The message entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $messageStorage;

  /**
   * The message notifier service.
   *
   * @var \Drupal\message_notify\MessageNotifier
   */
  protected $notifier;

  /**
   * Creates a new NotifierQueue object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $message_storage
   *   The message entity storage.
   * @param \Drupal\message_notify\MessageNotifier $notifier
   *   The message notifier service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityStorageInterface $message_storage, MessageNotifier $notifier) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->messageStorage = $message_storage;
    $this->notifier = $notifier;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('message'),
      $container->get('message_notify.sender')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    /** @var \Drupal\message\Entity\Message $message */
    $message = $this->messageStorage->load($data->message_id);
    if (empty($message)) {
      // The message no longer exists. Do nothing.
      return NULL;
    }

    if ($message->hasField('field_job_instance') && count($message->get('field_job_instance')->referencedEntities()) === 0) {
      // The job_instance no longer exists. Do nothing.
      return NULL;
    }

    // Send the message.
    // @throws \Drupal\message_notify\Exception\MessageNotifyException
    // If exception thrown then it will be caught higher up and requeued.
    $this->notifier->send($message, [], 'email');
  }
}

